CHCP 65001
@ECHO off


ECHO { "name": "TMEssential", "version": "1.0.0", "description": "", "main": "./TMEssential.js" } > package.json

IF EXIST TMEssential.js (
    ECHO T:true
) ELSE ( 
    ECHO "TMET Nodejs打包器未侦测到<TMEssential.js>文件!,无法正常工作
    GOTO exit 
)


7zr a -t7z TMET.llplugin TMEssential.js package.json

ECHO ""
ECHO ""
ECHO ""
ECHO ""
ECHO ""

ECHO "打包完成!将<TMET.llplugin>文件放入<plugins>目录开启服务器将自动安装

GOTO EXIT

:EXIT
DEL package.json
PAUSE