### Timiya系列插件交流&反馈群:1073980007
![LOGO图标](./logo.png)
# [注意事项]
## 该插件使用GPL-3.0协议发行,请遵照协议使用此插件！

***

##### 多语言目前只内置了zh_CN语言
##### 但是可以自定义语言包
##### 也欢迎来翻译制作语言包，制作完成发布至 [MineBBS](https://minebbs.net/) 即可

***

# [介绍]
##### 这是一个基于LiteLoaderScriptEngine的基础插件

***

# [功能一览]
* Economic_core (经济核心) 

* TPA (玩家互传)
* HOME (家)
* BACK (返回死亡点&记录死亡点)
* WARP (公共传送点)
* DynamicMotd (动态服务器名称)
* Notice (公告)
* Shop (商店)
* TPR (随机传送)
* RefreshChuck (刷新客户端区块)
* FarmLandProtect (耕地保护)

# [指令一览]

## 语言相关
* /language //打开语言切换GUI
* /language \<input:LangPackName> //切换语言

***

## 玩家互传相关
* /tpa //打开tpa面板
* /tpahere //同上
* /tpa gui //同上
* /tpa ui //开启或者关闭别人发来的请求ui
* /tpa to \<playerName:string> //请求传送到某人
* /tpahere to \<playerName:string> //请求某人传送到自己
* /tpaaccept //接受请求
* /tpadeny //拒绝某人的请求或者取消自己的请求

***

## 家传送点相关
* /home //打开home菜单
* /home ls //查看家列表
* /home go \<homeName:string> //去往某个家
* /home add \<homeName:string> //添加一个家
* /home del \<homeName:string>  //删除某个家
* /homeas //打开home管理菜单(op)
* /homeas \<playerName:string> ls //查看某人的家列表(op)
* /homeas \<playerName:string> go \<homeName:string> //去往某人的某个家(op)
* /homeas \<playerName:string> add \<homeName:string> //给某人添加一个家(op)
* /homeas \<playerName:string> del \<homeName:string> //让某人痛失一个家(op)

***

## 公共传送点相关
* /warp //打开warp菜单
* /warp add \<warpName:string> //添加一个传送点(op)
* /warp ls //列出传送点
* /warp go \<warpName:string> //去往某传送点
* /warp del \<warpName:string> //删除某传送点(op)

***

## 玩家死亡相关
* /back //前往最近的暴毙点
* /death //查看前几次的暴毙信息

***

## 经济相关
* /money //打开money表单
* /money gui //同上(llmoney模式请使用主命令)
* /money top //查看排行榜(llmoney模式使用llmoney的)
* /money hist \<playerName:string> //查看流水账(llmoney模式使用llmoney的)
* /money pay \<playerName:string> \<Num:int>//给某人打钱(llmoney模式使用llmoney的)
* /money query \[playerName:string\]//查询自己或者某人的经济(llmoney模式使用llmoney的)
* /money add \<playerName:string> \<Num:int> //给某人添加经济(op)(llmoney模式使用llmoney的)
* /money set \<playerName:string> <Num:int> //设置某人的经济(op)(llmoney模式使用llmoney的)
* /money reduce \<playerName:string> \<Num:int> //扣除某人的经济(op)(llmoney模式使用llmoney的)

***

## 公告相关
* /notice //打开公告
* /notice_op //编辑公告(op)

***

## 商店相关
* /shop //打开商店总表单
* /shop buy //打开买入表单
* /shop sell //打开卖出表单

***

## 控制台命令相关(热操作文件&语言包)

* /tmet reloaddata //重新从磁盘加载所有已加载数据文件
* /tmet lslangpack //列出语言包列表
* /tmet unloadlangpack \<LangPackName:string> //卸载已加载的语言包
* /tmet reloadlangpack \<LangPackName:string> //重新从磁盘加载已加载的语言包
* /tmet loadlangpack \<LangPackName:string> //从语言包列表加载语言包

***

## 随机传送相关
* /tpr //随机传送

***

## 刷新区块相关
* /rc //刷新区块

***

## [配置文件目录]
* .\\plugins\\Timiya\\config\\TMEssential.json

***

## [语言包目录]
* .\\plugins\\Timiya\\lang

***

+ [数据文件一览]
    - [LANGUAGE]
    .\\plugins\\Timiya\\data\\langsetting.json

    - [TPA]
    .\\plugins\\Timiya\\data\\tpasetting.json

    - [Home]
    .\\plugins\\Timiya\\data\\homelist.json

    - [Back]
    .\\plugins\\Timiya\\data\\deathlist.json

    - [WARP]
    .\\plugins\\Timiya\\data\\warplist.json

    - [Money]
    .\\plugins\\Timiya\\data\\offlineMoney.json

    - [Shop]
    .\\plugins\\Timiya\\data\\shopdata.json

***

# [配置说明]
``` json
{
    "Enable": true,//总开关
    "AutoUpdate": {//自动更新配置
        "Enable": true,//开关
        "AutoReload": true//在没有特殊情况下自动更新是否自动重载自身
    },
    "SelectForm": {//选择表单配置
        "Subsection": 40//一页表单最多多少个选择项
    },
    "Language": {//语言配置
        "Default": "zh_CN",//默认语言
        "Cmd": "language"//所注册的主命令
    },
    "TPA": {//玩家互传配置
        "Enable": true,//开关
        "ExpirationTime": 40,//超时时间(秒)
        "ConsumeMoney": 0//成功后的耗费经济
    },
    "WARP": {//地标配置
        "Enable": true,//开关
        "ConsumeMoney": 0//去一次设置的地标耗费经济
    },
    "Back": {//死亡点配置
        "Enable": true,//开关
        "MaxSave": 5,//一个玩家最大写入多少记录
        "SaveToFile": true,//是否保存至文件(重启后死亡点是否存在)
        "InvincibleTime": 5,//返回死亡点后的无敌时间(秒)
        "ConsumeMoney": 0//返回死亡点的耗费经济
    },
    "Home": {//家配置
        "Enable": true,//开关
        "MaxHome": 3,//最大可设置家数量
        "SaveRequiredMoney": 0,//设置家耗费经济
        "GoHomeRequiredMoney": 0,//前往家耗费经济
        "DelHomeBackOffMoney": 0//删除家返还经济
    },
    "Money": {//经济配置
        "Enable": true,//开关
        "MoneyType": "score",//经济模式(可选["score","llmoney"])
        "MoneyName": "money",//经济名称
        "PayTaxRate": 0.0,//转账税率(%)
        "HistoryLength": 10,//一个玩家的最大流水账记录次数(0关闭,-1无限)
        "MaxRankingQuantity": 100,//排行榜最大显示排行数量
        "MoneyChangeMsg": true,//经济变更提示开关
        "PlayerInitialMoney": 0//玩家初始经济(llmoney模式弃用)
    },
    "Notice": {//公告配置
        "Enable": true,//开关
        "JoinOpenNotice": true,//加入打开公告开关
        "NoticeTitle": "hello",//公告标题
        "NoticeText": "test"//公告内容
    },
    "Shop": {//商店配置
        "Enable": true//开关
    },
    "DynamicMotd": {//动态服务器消息展示配置
        "Enable": true,//开关
        "Time": 5,//变更时间(秒)
        "MotdList": [//Motd列表
            "test",
            "test2"
        ]
    },
    "TPR": {//随机传送配置
        "Enable": true,//开关
        "MaxXZCoordinate": 10000,//最大X和Z轴
        "MinXZCoordinate": -10000,//最小X和Z轴
        "ConsumeMoney": 0//耗费经济
    },
    "RefreshChunk": {//刷新客户端区块配置
        "Enable": true,//开关
        "ConsumeMoney": 0//消耗经济
    },
    "FarmLandProtect": {//耕地保护配置
        "Enable": true,//开关
        "Type": 0//保护类型(0全部拦截,1只拦截null对象造成耕地破坏,2只拦截非玩家破坏,3只拦截玩家破坏
    },
    "UseLog": {//使用日志配置
        "Enable": true,//开关
        "Conf": {//配置
            "API": false,
            "Language": true,
            "TPA": true,
            "WARP": true,
            "Back": true,
            "Home": true,
            "Money": true,
            "Notice": true,
            "Shop": true,
            "DynamicMotd": false,
            "TPR": true,
            "RefreshChunk": true,
            "FarmLandProtect": false
        }
    }
}
```

***

# [商店数据说明]
``` json
{
  "Buy": [//购买菜单
    {
      "type": "group",//类型【group为分类】
      "data": [//按钮数据【type为group为数组】
        {
          "name": "空气",//按钮名称
          "type": "exam",//类型【exam为商品】
          "data": {//数据【type为exam为物品对象】
            "type": "minecraft:air",//物品标准名
            "aux": 0,//物品特殊值
            "remark": "",//备注
            "money": 11//购买一个所需的经济
          }
        },
        {
          "name": "bread",//按钮名称
          "type": "exam",//类型【exam为商品】
          "data": {//数据【type为exam为物品对象】
            "type": "minecraft:bread",//物品标准名
            "aux": 0,//物品特殊值
            "remark": "",//备注
            "money": 2//购买一个所需的经济
          }
        }
     ]
    },
    {
      "name": "air",//按钮名称
      "type": "exam",//类型【exam为商品】
      "data": {//数据【type为exam为物品对象】
        "type": "minecraft:air",//物品标准名
        "aux": 0,//物品特殊值
        "remark": "",//备注
        "money": 11//购买一个所需的经济
      }
    }
  ],
  "Sell": [//卖出菜单
    {
      "name": "xx分类",//按钮名称
      "type": "group",//类型【group为分类】
      "data": [//按钮数据【type为group为数组】
        {
          "name": "空气",//按钮名称
          "type": "exam",//数据【type为exam为物品对象】
          "data": {//数据【type为exam为物品对象】
            "type": "minecraft:air",//物品标准名
            "aux": 0,//物品特殊值
            "remark": "",//备注
            "money": 11//回收一个获得得的经济
          }
        },
        {
          "name": "bread",//按钮名称
          "type": "exam",//数据【type为exam为物品对象】
          "data": {//数据【type为exam为物品对象】
            "type": "minecraft:bread",//物品标准名
            "aux": 0,//物品特殊值
            "remark": "",//备注
            "money": 1//回收一个获得得的经济
          }
        }
      ]
    },
    {
      "name": "redstone",//按钮名称
      "type": "exam",//数据【type为exam为物品对象】
      "data": {//数据【type为exam为物品对象】
        "type": "minecraft:redstone",//物品标准名
        "aux": 0,//物品特殊值
        "remark": "",//备注
        "money": 11//回收一个获得得的经济
      }
    }
  ]
}
```

***

## 如何进行TMETApi对接
* 新版本API依赖包[<u>点击前往下载</u>](https://gitee.com/timidine/mcbe-lite-loader-script-engine-tmessential/blob/main/TMET%E6%96%B0%E7%89%88%E6%9C%AC%E6%8F%92%E4%BB%B6api%E8%B0%83%E7%94%A8%E5%AE%9E%E4%BE%8B%E5%92%8C%E5%BC%80%E5%8F%91%E4%BE%9D%E8%B5%96%E5%8C%85.zip)