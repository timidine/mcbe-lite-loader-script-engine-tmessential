# 如何让插件强制在LL3Beta版运行
> 注意!在此模式下TMET的远程调用无法使用!因为LL3Beta版的远程调用暂时无法正常工作！
* 因为暂时LL3LL3Beta版的获取插件API无法使用,所以要做以下操作
* 在此声明,此插件在LL3Beta版环境下运行所产生的问题作者都不会负责!
## 第一步:
* 创建在BDS根目录的文件夹: ./plugins/timiya/config
## 第二步:
* 在刚创建的文件夹里创建文件: TMET_LL3ForceLoadConfig.json
## 第三步:
* 在文件里写
``` json
{
    "TMETRunPath": "{你的TMET的JS文件目录,可以填绝对目录和相对目录}"
}
```
* 例如
``` json
{
    "TMETRunPath": "./plugins/TMEssential/TMEssential.js"
}
```
* 保存此文件再开服
