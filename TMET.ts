// LiteLoader-AIDS automatic generated
/// <reference path="c:\Users\Administrator\.vscode\extensions\moxicat.llscripthelper-2.1.5/dts/llaids/src/index.d.ts"/>


class TMET_Event<T extends (...args: any[]) => boolean> {
    private Funcs = new Map<string, T>();
    private AfterFuncs = new Map<string, T>();
    constructor(private name: string) { }
    /** 添加监听器(Before) */
    addBefore(name: string, func: T) {
        if (this.Funcs.has(name)) {
            logger.warn(`事件"${this.name}"添加Before监听器"${name}"失败!已有一个叫这名字的监听器了!`);
            return false;
        }
        this.Funcs.set(name, func);
        return true;
    }
    /** 删除Before事件 */
    delBefore(name: string) {
        return this.Funcs.delete(name);
    }
    /** 添加监听器(After),如果在Before事件已经拦截,将不会触发此事件 */
    addAfter(name: string, func: T) {
        if (this.AfterFuncs.has(name)) {
            logger.warn(`事件"${this.name}"添加After监听器"${name}"失败!已有一个叫这名字的监听器了!`);
            return false;
        }
        this.AfterFuncs.set(name, func);
        return true;
    }
    /** 删除After事件 */
    delAfter(name: string) {
        return this.AfterFuncs.delete(name);
    }
    /** 触发事件 */
    ___TriggerEvent(...args: Parameters<T>): boolean {

        return true;
    }
}

//重要API封装与此
//#region TMET_Global
namespace TMET_Global {
    export function TryToRun(fn: () => void, name: string) {
        try {
            fn();
        } catch (e) {
            logger.error(`Error in ${name}`);
            PrintErrorMsg(e);
        }
    }

    function PrintErrorMsg(error: any) {
        let ErrorMsg = "Unknown";
        let ErrorName = "UnknownError";
        let ErrorStack = "";
        if (error instanceof Error) {
            ErrorName = error.name || ErrorName;
            ErrorStack = error.stack || ErrorStack;
            if (ErrorStack.indexOf(ErrorName) == 0) {
                logger.error(ErrorStack);
                return;
            }
            ErrorMsg = error.message;
        }
        logger.error(`${ErrorName}: ${ErrorMsg}\n${ErrorStack}`);
    }
    let TickTasks = new Set<[string, () => void]>();
    export let MC_Events = {
        "onPreJoin": new Map<string, (pl: Player) => void>(),
        "onJoin": new Map<string, (pl: Player) => void>()
    };

    export function AddTickTask(name: string, task: () => void) {
        return TickTasks.add([name, task]);
    }
    export function DeleteTickTask(task: [string, () => void]) {
        return TickTasks.delete(task);
    }

}
//#endregion



function ListeningEvents() {
}

//---启动函数---
function main() {
}


main();