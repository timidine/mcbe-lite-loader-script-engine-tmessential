/**
 * @note 这是一个远程调用API模板
 * @note 它可以自动下载依赖，所以可以不装lib,最好是装上lib，因为它没办法保持最新!
 */


function onLoadedLib() {
    TMETEvent.onMotdChange("Test", function onMotdChange(str) {//尽量不使用匿名函数
        logger.info(`Motd Change to "${str}"`);
        return true;//返回false拦截
    });
    TMETEvent.onTPRTry("Test", (xuid, x, z) => {//如果使用匿名函数将自动将此函数赋予一个随机别名
        let pl = mc.getPlayer(xuid);
        let BeforePos = pl.pos;
        logger.info(`Player: ${pl.realName} Pos:(${BeforePos.toString()}) TryTPR to Pos(x:${x}, z:${z})`);
        logger.info(`This Player Have Money: ${TMETApi.getMoney(pl.realName)} ${TMETApi.getMoneyName()}`);
        //不返回默认true
    });
    logger.info("TMET RemoteCall API Template Loaded!");
}

(() => {//Lib
    if (!File.exists("./plugins/lib/TMETLib.js")) {
        let str = null;
        network.httpGet("https://gitee.com/timidine/mcbe-lite-loader-script-engine-tmessential/raw/main/plugin/lib/TMETLib.js", (code, res) => {
            if (code != 200) {
                throw new Error("Getting dependencies through the network: \"TMETLib\" failed!");
            }
            File.writeTo("./plugins/lib/TMETLib.js", res.replace(/\r/g, ""));
            require("./lib/TMETLib.js");
            onLoadedLib();
        });
    } else {
        require("./lib/TMETLib.js");
        onLoadedLib();
    }
})();