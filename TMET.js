"use strict";
// LiteLoader-AIDS automatic generated
/// <reference path="c:\Users\Administrator\.vscode\extensions\moxicat.llscripthelper-2.1.5/dts/llaids/src/index.d.ts"/>
class TMET_Event {
    constructor(name) {
        this.name = name;
        this.Funcs = new Map();
        this.AfterFuncs = new Map();
    }
    /** 添加监听器(Before) */
    addBefore(name, func) {
        if (this.Funcs.has(name)) {
            logger.warn(`事件"${this.name}"添加Before监听器"${name}"失败!已有一个叫这名字的监听器了!`);
            return false;
        }
        this.Funcs.set(name, func);
        return true;
    }
    /** 删除Before事件 */
    delBefore(name) {
        return this.Funcs.delete(name);
    }
    /** 添加监听器(After),如果在Before事件已经拦截,将不会触发此事件 */
    addAfter(name, func) {
        if (this.AfterFuncs.has(name)) {
            logger.warn(`事件"${this.name}"添加After监听器"${name}"失败!已有一个叫这名字的监听器了!`);
            return false;
        }
        this.AfterFuncs.set(name, func);
        return true;
    }
    /** 删除After事件 */
    delAfter(name) {
        return this.AfterFuncs.delete(name);
    }
    /** 触发事件 */
    ___TriggerEvent(...args) {
        return true;
    }
}
//重要API封装与此
//#region TMET_Global
var TMET_Global;
(function (TMET_Global) {
    function TryToRun(fn, name) {
        try {
            fn();
        }
        catch (e) {
            logger.error(`Error in ${name}`);
            PrintErrorMsg(e);
        }
    }
    TMET_Global.TryToRun = TryToRun;
    function PrintErrorMsg(error) {
        let ErrorMsg = "Unknown";
        let ErrorName = "UnknownError";
        let ErrorStack = "";
        if (error instanceof Error) {
            ErrorName = error.name || ErrorName;
            ErrorStack = error.stack || ErrorStack;
            if (ErrorStack.indexOf(ErrorName) == 0) {
                logger.error(ErrorStack);
                return;
            }
            ErrorMsg = error.message;
        }
        logger.error(`${ErrorName}: ${ErrorMsg}\n${ErrorStack}`);
    }
    let TickTasks = new Set();
    TMET_Global.MC_Events = {
        "onPreJoin": new Map(),
        "onJoin": new Map()
    };
    function AddTickTask(name, task) {
        return TickTasks.add([name, task]);
    }
    TMET_Global.AddTickTask = AddTickTask;
    function DeleteTickTask(task) {
        return TickTasks.delete(task);
    }
    TMET_Global.DeleteTickTask = DeleteTickTask;
})(TMET_Global || (TMET_Global = {}));
//#endregion
function ListeningEvents() {
}
//---启动函数---
function main() {
}
main();
